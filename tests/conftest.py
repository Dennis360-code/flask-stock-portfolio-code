import pytest
from project import create_app, database
from project.models import Stock
from flask import current_app

@pytest.fixture(scope="function")
def test_client():
    flask_app = create_app()
    flask_app.config.from_object("config.TestingConfig")

    # Create a test client using the Flask application configured for testing
    with flask_app.test_client() as testing_client:
        # Establish an application context
        with flask_app.app_context():
            current_app.logger.info("In the test_client() fixture...")

            # Create the database and the database table(s)
            database.create_all()

        yield testing_client

        with flask_app.app_context():
            database.drop_all()

@pytest.fixture(scope="module")
def new_stock():
    stock = Stock("AAPL", "16", "406.78")
    return stock
